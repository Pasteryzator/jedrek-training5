package main.java.pl.sda;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj tekst, którego trzy ostatnie litery chcesz zamienić.");
        String input = scanner.nextLine();
        System.out.println(input);

        int x = input.length();

        String firstBit = input.substring(0, x-3);
        String secondBit = input.substring(x-3, x);
        String capitalizedBit = secondBit.toUpperCase();
        String finalText = firstBit + capitalizedBit;

        System.out.println(finalText);

    }
}
